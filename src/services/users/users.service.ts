import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import * as argon2 from 'argon2';
import { ResponseUserDto } from './dto/response-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  public async createUser(
    createUserDto: CreateUserDto,
  ): Promise<ResponseUserDto> {
    try {
      const password = { ...createUserDto };
      const hashPassword = await argon2.hash(password.password);
      createUserDto.password = hashPassword;
      return await this.userRepository.save(createUserDto);
    } catch (e) {
      throw e;
    }
  }

  public async findOneUserByEmail(email: string): Promise<ResponseUserDto> {
    try {
      return await this.userRepository.findOne({ where: { email } });
    } catch (e) {
      throw e;
    }
  }

  public async findUserById(id: string): Promise<ResponseUserDto> {
    try {
      return await this.userRepository.findOne({ where: { id } });
    } catch (e) {
      throw e;
    }
  }
}
