import { Column, Entity } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'users' })
export class User extends CoreEntity {
  @ApiProperty({
    example: 'someemail@gmail.com',
    type: String,
    description: 'The email of the user',
  })
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @ApiProperty({
    example: '123d456',
    type: String,
    description: 'The password of the user',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;
}
