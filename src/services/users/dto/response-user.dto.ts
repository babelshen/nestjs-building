import { IsDate, IsEmail, IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResponseUserDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: 'b0e31418-e591-43c5-bbe5-de4990a519d7',
    type: String,
    description: 'UUID of the user',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the user',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the user',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'someemail@gmail.com',
    type: String,
    description: 'The email of the user',
    minLength: 3,
    maxLength: 40,
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '123d456',
    type: String,
    description: 'The password of the user',
    minLength: 6,
    maxLength: 20,
  })
  password: string;
}
