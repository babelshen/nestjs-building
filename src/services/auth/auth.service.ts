import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SignResponseDto } from '../../controllers/auth/dto/sign.response.dto';
import * as argon2 from 'argon2';
import { SignUpRequestDto } from '../../controllers/auth/dto/sign-up.request.dto';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UsersService,
  ) {}

  public async signUp(signUpDto: SignUpRequestDto): Promise<SignResponseDto> {
    const user = {
      email: signUpDto.email,
      password: signUpDto.password,
    };
    const userInfo = await this.userService.createUser(user);
    const payload = { userId: userInfo.id, userEmail: userInfo.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async signIn(
    email: string,
    password: string,
  ): Promise<SignResponseDto> {
    try {
      const user = await this.userService.findOneUserByEmail(email);

      if (!user) {
        throw new UnauthorizedException('Incorrect email or password');
      }
      const verification = await argon2.verify(user.password, password);
      if (!verification) {
        throw new UnauthorizedException('Incorrect email or password');
      }
      const payload = { userId: user.id, userEmail: user.email };
      return {
        accessToken: await this.jwtService.signAsync(payload),
      };
    } catch (e) {
      throw e;
    }
  }
}
