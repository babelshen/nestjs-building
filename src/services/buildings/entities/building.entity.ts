import { Column, Entity } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'buildings' })
export class Building extends CoreEntity {
  @ApiProperty({
    example: 'The Marina Torch',
    type: String,
    description: 'The name of the building',
  })
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  name: string;

  @ApiProperty({
    example: 'https://i16.servimg.com/u/f16/20/20/43/41/marina10.png',
    type: String,
    description: 'The link on the image',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  imageLink: string;

  @ApiProperty({
    example: '6500000 Dhs',
    type: String,
    description: 'Cost field',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  cost: string;

  @ApiProperty({
    example: '60000 Dhs',
    type: String,
    description: 'Ticket field',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  ticket: string;

  @ApiProperty({
    example: '9.25%',
    type: String,
    description: 'Yield field',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  yield: string;

  @ApiProperty({
    example: 150,
    type: Number,
    description: 'Days field',
  })
  @Column({
    type: 'integer',
    nullable: false,
  })
  days: number;

  @ApiProperty({
    example: '75%',
    type: String,
    description: 'Sold field',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  sold: string;
}
