import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Building } from './entities/building.entity';
import { CreateBuildingDto } from './dto/create-building.dto';
import { UpdateBuildingDto } from './dto/update-building.dto';
import { ResponseBuildingDto } from './dto/response-building.dto';

@Injectable()
export class BuildingsService {
  constructor(
    @InjectRepository(Building)
    private readonly buildingRepository: Repository<Building>,
  ) {}

  public async createBuilding(
    createBuildingDto: CreateBuildingDto,
  ): Promise<ResponseBuildingDto> {
    try {
      return await this.buildingRepository.save(createBuildingDto);
    } catch (e) {
      throw e;
    }
  }

  public async getAllBuildings(): Promise<ResponseBuildingDto[]> {
    try {
      return await this.buildingRepository.find();
    } catch (e) {
      throw e;
    }
  }

  public async updateBuilding(
    id: string,
    updateBuildingDto: UpdateBuildingDto,
  ): Promise<ResponseBuildingDto> {
    try {
      await this.buildingRepository.update(id, updateBuildingDto);
      const result = await this.buildingRepository.findOne({ where: { id } });

      if (!result) {
        throw new NotFoundException('Building not found');
      }
      return result;
    } catch (e) {
      throw e;
    }
  }

  public async removeBuilding(id: string): Promise<ResponseBuildingDto> {
    try {
      const result = this.buildingRepository.findOne({ where: { id } });

      if (!result) {
        throw new NotFoundException('Building not found');
      }
      await this.buildingRepository.delete(id);
      return result;
    } catch (e) {
      throw e;
    }
  }
}
