import {
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResponseBuildingDto {
  @IsUUID()
  @IsNotEmpty()
  @ApiProperty({
    example: 'b0e31418-e591-43c5-bbe5-de4990a519d7',
    type: String,
    description: 'UUID of the building',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the building',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the building',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'The Marina Torch',
    type: String,
    description: 'The name of the building',
    minLength: 5,
    maxLength: 50,
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'https://i16.servimg.com/u/f16/20/20/43/41/marina10.png',
    type: String,
    description: 'The link on the image',
    minLength: 5,
  })
  imageLink: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '6500000 Dhs',
    type: String,
    description: 'Cost field',
    minLength: 5,
  })
  cost: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 150,
    type: Number,
    description: 'Days field',
  })
  days: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '9.25%',
    type: String,
    description: 'Yield field',
    minLength: 2,
  })
  yield: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '75%',
    type: String,
    description: 'Sold field',
    minLength: 2,
  })
  sold: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '60000 Dhs',
    type: String,
    description: 'Ticket field',
    minLength: 5,
  })
  ticket: string;
}
