import {
  IsNotEmpty,
  IsNumber,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateBuildingDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(5, {
    message: 'The name must contain a minimum of three characters',
  })
  @MaxLength(50, {
    message: 'The name should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'The Marina Torch',
    type: String,
    description: 'The name of the building',
    minLength: 5,
    maxLength: 50,
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(5, {
    message: 'Your link is too short',
  })
  @ApiProperty({
    example: 'https://i16.servimg.com/u/f16/20/20/43/41/marina10.png',
    type: String,
    description: 'The link on the image',
    minLength: 5,
  })
  imageLink: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(5, {
    message: 'This field cannot contain less than five characters',
  })
  @ApiProperty({
    example: '6500000 Dhs',
    type: String,
    description: 'Cost field',
    minLength: 5,
  })
  cost: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1, {
    message: 'The value of this field cannot be less than 1',
  })
  @Max(365, {
    message: 'The value of this field cannot be more than 365',
  })
  @ApiProperty({
    example: 150,
    type: Number,
    description: 'Days field',
  })
  days: number;

  @IsNotEmpty()
  @IsString()
  @MinLength(2, {
    message: 'This field cannot contain less than 2 characters',
  })
  @ApiProperty({
    example: '9.25%',
    type: String,
    description: 'Yield field',
    minLength: 2,
  })
  yield: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(2, {
    message: 'This field cannot contain less than 2 characters',
  })
  @ApiProperty({
    example: '75%',
    type: String,
    description: 'Sold field',
    minLength: 2,
  })
  sold: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(5, {
    message: 'This field cannot contain less than five characters',
  })
  @ApiProperty({
    example: '60000 Dhs',
    type: String,
    description: 'Ticket field',
    minLength: 5,
  })
  ticket: string;
}
