import { Injectable, NotFoundException } from '@nestjs/common';
import { UsersService } from 'src/services/users/users.service';

@Injectable()
export class UsersControllerService {
  constructor(private readonly usersService: UsersService) {}

  public async findCurrentUser(id: string): Promise<any> {
    const user = await this.usersService.findUserById(id);

    if (!user) {
      throw new NotFoundException(`User is not found`);
    }

    return { id: user.id, email: user.email };
  }
}
