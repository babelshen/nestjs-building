import { Controller, Get, HttpStatus, UseGuards } from '@nestjs/common';
import { UsersControllerService } from './users.controller.service';
import { CurrentUser } from 'src/services/auth/decorators/current.user.decorators';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { ResponseUserDto } from 'src/services/users/dto/response-user.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserResponseDto } from './dto/user-response.dto';
import { LoggedUserDto } from './dto/logged-user.dto';

@ApiTags('User')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@Controller('users')
@ApiBearerAuth('jwt')
export class UsersController {
  constructor(
    private readonly usersControllerService: UsersControllerService,
  ) {}

  @ApiOperation({ summary: 'Return user information' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Request recieve information about user',
    type: UserResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: {
        message: 'Unauthorized',
        statusCode: 401,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @Get('me')
  public async findMe(
    @CurrentUser()
    loggedUser: LoggedUserDto,
  ): Promise<ResponseUserDto> {
    return await this.usersControllerService.findCurrentUser(loggedUser.id);
  }
}
