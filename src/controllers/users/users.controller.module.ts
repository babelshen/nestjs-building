import { Module } from '@nestjs/common';
import { UsersModule } from 'src/services/users/users.module';
import { UsersController } from './users.controller';
import { UsersControllerService } from './users.controller.service';

@Module({
  imports: [UsersModule],
  controllers: [UsersController],
  providers: [UsersControllerService],
})
export class UsersControllerModule {}
