import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { BuildingsControllerService } from './buildings.controller.service';
import { CreateBuildingDto } from 'src/services/buildings/dto/create-building.dto';
import { UpdateBuildingDto } from 'src/services/buildings/dto/update-building.dto';
import {
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ResponseBuildingDto } from 'src/services/buildings/dto/response-building.dto';

@ApiTags('Buildings')
@Controller('buildings')
export class BuildingsController {
  constructor(
    private readonly buildingsControllerService: BuildingsControllerService,
  ) {}

  @ApiOperation({ summary: 'Create article about building' })
  @ApiBody({
    type: CreateBuildingDto,
    description: 'Create article request',
    examples: {
      based: {
        summary: 'Example of creating an article',
        value: {
          name: 'The Marina Torch',
          imageLink: 'https://i16.servimg.com/u/f16/20/20/43/41/marina10.png',
          cost: '6500000 Dhs',
          days: 150,
          yield: '9.25%',
          sold: '75%',
          ticket: '60000 Dhs',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: ResponseBuildingDto,
    description: 'Information about the created article',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation error',
    schema: {
      example: {
        message: ['name must be a string'],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  createBuilding(
    @Body() createBuildingDto: CreateBuildingDto,
  ): Promise<ResponseBuildingDto> {
    return this.buildingsControllerService.createBuild(createBuildingDto);
  }

  @ApiOperation({ summary: 'Get articles about all buildings' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: ResponseBuildingDto,
    isArray: true,
    description: 'List of articles',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @Get()
  findAllBuildings(): Promise<ResponseBuildingDto[]> {
    return this.buildingsControllerService.findAllBuildings();
  }

  @ApiOperation({ summary: 'Update information about building' })
  @ApiBody({
    description: 'Request for update information',
    examples: {
      based: {
        summary: 'Example of request',
        value: {
          name: 'HHHR Tower',
        },
      },
    },
  })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the building',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update information',
    type: ResponseBuildingDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid id | Validation failed',
    schema: {
      example: {
        message: 'Validation failed (uuid is expected)',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Building not found',
    schema: {
      example: {
        message: 'Building not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @Patch(':id')
  updateBuilding(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateBuildingDto: UpdateBuildingDto,
  ): Promise<ResponseBuildingDto> {
    return this.buildingsControllerService.updateBuilding(
      id,
      updateBuildingDto,
    );
  }

  @ApiOperation({ summary: 'Delete building' })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the building',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Building was delete',
    type: ResponseBuildingDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid building id',
    schema: {
      example: {
        message: 'Validation failed (uuid is expected)',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Building not found',
    schema: {
      example: {
        message: 'Building not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  removeBuilding(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<ResponseBuildingDto> {
    return this.buildingsControllerService.removeBuilding(id);
  }
}
