import { Module } from '@nestjs/common';
import { BuildingsModule } from 'src/services/buildings/building.module';
import { BuildingsController } from './buildings.controller';
import { BuildingsControllerService } from './buildings.controller.service';

@Module({
  imports: [BuildingsModule],
  controllers: [BuildingsController],
  providers: [BuildingsControllerService],
})
export class BuildingsControllerModule {}
