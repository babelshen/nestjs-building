import { Injectable } from '@nestjs/common';
import { BuildingsService } from 'src/services/buildings/buildings.service';
import { CreateBuildingDto } from 'src/services/buildings/dto/create-building.dto';
import { UpdateBuildingDto } from 'src/services/buildings/dto/update-building.dto';

@Injectable()
export class BuildingsControllerService {
  constructor(private readonly buildingsService: BuildingsService) {}

  createBuild(createBuildingDto: CreateBuildingDto) {
    return this.buildingsService.createBuilding(createBuildingDto);
  }

  findAllBuildings() {
    return this.buildingsService.getAllBuildings();
  }

  updateBuilding(id: string, updateBuildingDto: UpdateBuildingDto) {
    return this.buildingsService.updateBuilding(id, updateBuildingDto);
  }

  removeBuilding(id: string) {
    return this.buildingsService.removeBuilding(id);
  }
}
