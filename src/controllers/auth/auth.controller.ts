import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from '../../services/auth/auth.service';
import { SignUpRequestDto } from './dto/sign-up.request.dto';
import { SignResponseDto } from './dto/sign.response.dto';
import { SignInRequestDto } from './dto/sign-in.request.dto';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Sign in' })
  @ApiBody({
    type: SignInRequestDto,
    description: 'Sign in form',
    examples: {
      based: {
        summary: 'Example of sign in form',
        value: {
          email: 'someemail@gmail.com',
          password: '123d45',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation error',
    schema: {
      example: {
        message: ['email must be an email'],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Account not found',
    schema: {
      example: {
        message: 'Incorrect email or password',
        error: 'Unauthorized',
        statusCode: 401,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  public signIn(@Body() signInDto: SignInRequestDto): Promise<SignResponseDto> {
    return this.authService.signIn(signInDto.email, signInDto.password);
  }

  @ApiOperation({ summary: 'Sign up' })
  @ApiBody({
    type: SignUpRequestDto,
    description: 'Sign up form',
    examples: {
      based: {
        summary: 'Example of sign up form',
        value: {
          email: 'someemail@gmail.com',
          password: '123d45',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Dublicate e-mail | Validation failed',
    schema: {
      example: {
        message: 'User with this e-mail already exists',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: {
        statusCode: 500,
        message: 'Internal Server Error',
      },
    },
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-up')
  public signUp(@Body() signUpDto: SignUpRequestDto): Promise<SignResponseDto> {
    return this.authService.signUp(signUpDto);
  }
}
