import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignInRequestDto {
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3, {
    message: 'The email must contain a minimum of three characters',
  })
  @MaxLength(40, {
    message: 'The email should not be more than fourty characters',
  })
  @ApiProperty({
    example: 'someemail@gmail.com',
    type: String,
    description: 'The email of the user',
    minLength: 3,
    maxLength: 40,
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6, {
    message: 'The password must contain a minimum of six characters',
  })
  @MaxLength(20, {
    message: 'The password should not be more than twenty characters',
  })
  @ApiProperty({
    example: '123d456',
    type: String,
    description: 'The password of the user',
    minLength: 6,
    maxLength: 20,
  })
  password: string;
}
