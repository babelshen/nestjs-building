import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { ConfigModule } from '../configs/config.module';
import { BuildingsModule } from 'src/services/buildings/building.module';
import { BuildingsControllerModule } from 'src/controllers/buildings/buildings.controller.module';
import { UsersModule } from 'src/services/users/users.module';
import { UsersControllerModule } from 'src/controllers/users/users.controller.module';
import { AuthControllerModule } from '../controllers/auth/auth.controller.module';
import { AuthModule } from '../services/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    UsersModule,
    UsersControllerModule,
    BuildingsModule,
    BuildingsControllerModule,
    AuthControllerModule,
    AuthModule,
  ],
})
export class AppModule {}
